/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on October 8, 2019, 6:50 PM
 */

#include <cstdlib>

using namespace std;

class Car
{
private:
    int miles;
public:
    Car();
    Car(const Car &);
    ~Car();
    const Car &operator = (const Car&);
    Car operator ++(); // pre-fix
    Car operator ++(int); //post-fix
};

//Prefix
Car Car::operator ++()
{
    ++miles;
    return *this;
}

//Post-Fix
Car Car::operator ++(int)
{
    Car temp(*this);
    ++miles;
    return temp;
}

/*
 * 
 */
int main(int argc, char** argv) {
    
    
    

    return 0;
}

