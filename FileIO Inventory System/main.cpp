/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on October 10, 2019, 7:15 PM
 */

#include <cstdlib>
#include <iostream>
#include <fstream>

using namespace std;


struct InventoryItem
{
    double cost;
    char name[100];
    int id;
};


void printIventoryItem(const InventoryItem& item)
{
    cout << "Name: " << item.name << endl;
    cout << "Cost: " << item.cost << endl;
    cout << "ID: " << item.id << endl;
}

/*
 * 
 */
int main(int argc, char** argv) {
    cout << "Would you like to make a base file?";
    string input;
    cin >> input;
    
    if (tolower(input[0]) == 'y')
    {
    
    //Create 3 inventory items, Write to binary file.
    InventoryItem banana = { .39, "banana", 10};
    InventoryItem apple = { .50, "apple", 20 };
    InventoryItem candy = { 1.25, "twix", 2000 };
    
    // Ask the user for a fileName
    
    cout << "Please enter a file name: ";
    string filename;
    cin >> filename;
    
    
    
    fstream file;
    
    file.open(filename.c_str(),ios::out | ios::binary);
    file.write(reinterpret_cast<char*> (&banana), sizeof(InventoryItem));
    file.write(reinterpret_cast<char*> (&apple), sizeof(InventoryItem));
    file.write(reinterpret_cast<char*> (&candy), sizeof(InventoryItem));
    
    file.close();
    }
    else
    {
        cout << "Enter a file to read from: ";
        string filename;
        cin >> filename;
        
        fstream file;
        file.open(filename.c_str(), ios::in | ios::out | ios::binary);
        
        //Error checing
        while (file.fail())
        {
            cout << "file not valid! Enter a new file: ";
            cin >> filename;
            file.open(filename.c_str(), ios::in | ios::out | ios::binary);
        }
        
        //counter
        int numItems = 0;
        
        //Get number of inventory items
        while (!file.eof())
        {
            //Create a tempary inventory item to read into
            InventoryItem temp;
            file.read(reinterpret_cast <char*> (&temp), sizeof(InventoryItem));
            numItems ++;
        }
        
        
        cout << "There are " << numItems << " Items";
        
        
        cout << "Would you like to modify an item?";
        cin >> input;
        
        if (tolower(input[0]) == 'y')
        {
            cout << "Which item number would you like to change?";
            int itemNumber;
            cin >> itemNumber;
            
            //TODO - Error check that the item number is valid
            
            //Find the correct write location
            file.seekp(itemNumber * sizeof(InventoryItem), ios::beg);
            
            //Get the value from the user
            InventoryItem newItem;
            cout << "Enter a new name: ";
            cin >> newItem.name;
            
            cout << "Enter a new cost: ";
            cin >> newItem.cost;
            
            cout << ""
            
        }
    }

    return 0;
}

