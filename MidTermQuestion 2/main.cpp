/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on October 10, 2019, 6:12 PM
 */

#include <cstdlib>

using namespace std;


class Student
{
private:
    string* name;
    int age;
public:
    Student();
    ~Student();
    //Write and Define missing Functions
    Student(const Student&);
    const Student& operator=(const Student &);
};

//Copy Constructor

Student::Student(const Student &student)
{
    age = student.age;
    name = new string(*(student.name));
}

const Student & Student::operator =(const Student& rhs)
{
    //Self - Check
    if(this == &rhs)
        return *this;
    this->age = rhs.age;
    //deallocate
    delete name;
    //Re-allocate
    this->name = new string (*(rhs.name));
    return *this;
}

/*
 * 
 */
int main(int argc, char** argv) {

    return 0;
}

