/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on October 10, 2019, 7:01 PM
 */

#include <cstdlib>

using namespace std;



/*
 * RANDOM ACCESS
 * fstream file;
 * file.open ("test.txt");
 * string val;
 * //Move read pointer using seekg(#chars, starting location)
 * file.seekg(6L, ios::beg);
 * 
 * USE
 * ios::beg -> Beginning
 * ios::cur -> Current Spot
 * ios::end -> End of file
 */
int main(int argc, char** argv) {

    return 0;
}

