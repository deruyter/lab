/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on October 10, 2019, 6:26 PM
 */

#include <cstdlib>

using namespace std;

/*
 FILE IO
 * 
 * 1) Open the File
 * 2) Manipulate/Access File
 * 3) Close the file
 * 
 ERRORS
 * 
 * Reading a File
 * 
 * ifstream infile;
 * infile.open("test.txt");
 * 
 * Validate
 * 
 * if(infile.fail())
 * {
 * cout << "Invalid File";
 * }
 * 
 * Get File name from user
 * 
 * cout << "Enter a file name";
 * string fileName;
 * cin >> Filename;
 * 
 * ifstream infile;
 * infile.open(fileName.c_str());
 * 
 * while( infile.fail())
 * {
 * cout << "File Failed! Enter again: ";
 * cin >> fileName;
 * infile.open(fileName.c_str());
 * }
 * 
 * 
 * 
 * INPUT AND OUTPUT BUFFER
 * fstream file(ios::in | ios::out);
 * 
 * file.open("test.txt");
 * file << "Hello";
 * string x;
 * file >> x;
 * 
 * file.close(); //File is Saved
 * 
 * 
 * 
 * FLAGS
 * ios::in -> input
 * ios::out -> output
 * ios::app -> appending
 * ios::binary -> binary file (good for structs)
 * 
 * 
 * 
 * WRITING TO BINARY FILE
 * fstream File(ios::out | ios::binary);
 * file.open("data.dat");
 * //No stream operator
 * //Use write function
 * char c;
 * c = 'c';
 * file.write(&c,sizeof(c));
 * int x = 20;
 * file.write(&x,sizeof(int));
 * 
 * double val = 10.32;
 * //Cast pointer to char pointer using reinterpret_cast
 * file.write(reinterpret_cast < char* >(&val),sizeof(double));
 */
  struct Person
  {
  int age;
  double weight;
  char name [100];
  };




/*
 * 
 */
int main(int argc, char** argv) {
    
    Person p1 = { 10, 200.00, "Dave"};
    Person p2;
    Person p3;
    
    //Binary file
    fstream file(ios::out | ios::binary);
    file.open("Person.dat");
    file.write(reinterpret_cast <char*>(&p1), sizeof(Person));
    file.write(reinterpret_cast <char*> (&p2), sizeof (Person));
    file.close();
    
    ifstream infile(ios::binary);
    infile.open ("Person.dat");
    infile.read(reinterpret_cast<char*>(&p3), sizeof(Person));

    return 0;
}

