/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on October 8, 2019, 6:05 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;


class Person{
private:
    string name;
    double* bloodCount;
public:
    Person (); //Default
    Person (const Person&); //Copy
    ~Person(); //Destructor
    const Person & operator = (const Person&); // Assignment overload
    string getName();
    void setName(string);
    friend ostream &operator << (ostream&, const Person&);
};

Person::Person()
{
    name = "";
    bloodCount = new double;
}

Person::Person(const Person &person)
{
    this->name = person.name;
    this->bloodCount = new double(*(person.bloodCount));
}


Person::~Person(){
    delete bloodCount;
}

string Person::getName(){
    return name;
}

void Person::setName(string name)
{
    this->name = name;
}

const Person& Person::operator =(const Person &rhs)
{
    //self-check
    if(this == &rhs) return *this;
    this->name = rhs.name;
    //deallocate
    delete bloodCount;
    this->bloodCount = new double (*(rhs.bloodCount));
    
    return *this;
}

ostream& operator << (ostream & out, const Person & person)
{
    out << "Name:" << person.name << endl;
    out << "Blood Count: " << *(person.bloodCount) << endl;
}

/*Person(string n, double* b)
{
    string n = "";
    double b = 0;
}*/

/*
 * 
 */
int main(int argc, char** argv) {
    
    Person p;  //Default constructor
    
    Person matt = p; // Copy constructor
    
    Person dave; //Default constructor
    
    dave = p; // Assignment overload
    
    cout << p << matt << dave;
  
    
    
    

    return 0;
}

