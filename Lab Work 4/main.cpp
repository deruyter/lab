/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on September 19, 2019, 7:52 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

class Item
{
private:
    string desc;
    int units;
    double price;
public:
    Item();
    Item(string desc, int units, double price);
    
    int getDesc();
    void setDesc(string desc);
    
    int getUnits();
    void setUnits(int units);
    
    int getPrice();
    void setPrice();
    
    void output();
    
};

Item::Item()
{
    desc = "";
    units = 0;
    price = 0;
}

Item::Item(string desc, int units, double price)
{
    this->desc = desc;
    this->price = price;
    this->units = units;
}

void Item::output()
{
    cout << "Description: " << desc << endl;
    cout << "Price: " << price << endl;
    cout << "Units: " << units << endl;
}
        



/*
 * 
 */
int main(int argc, char** argv) {
    
    Item Jacket("Jacket", 12, 59.95);
    Jacket.output();

    return 0;
}

