/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on September 12, 2019, 7:15 PM
 */

#include <cstdlib>
#include <fstream> // File IO
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    //Step 1: OPen the file
    
    ofstream outfile;
    outfile.open("test.txt"); // Creates the file
    
    //Step 2: Access/Write to file
    //Write to the file, just like cout
    outfile << "Hello, I am being written to a file";
    
    //Step 3: Close the file
    
    outfile.close();
    
    
    // Read from file
    ifstream infile;
    
    //Step 1
    infile.open("test.txt");
    
    //Step 2
    //Read all text one word at a time
    string input;
    while(infile >> input){
        cout << input << endl;
    }
    
    //Step 3
    infile.close();

    return 0;
}

