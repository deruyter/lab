/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on October 8, 2019, 7:16 PM
 */

#include <cstdlib>

using namespace std;

class Student
{
private:
    int numBooks;
    int* bookIDs;
public:
    //subscript overload
    int& operator[](int);
};

int & Student::operator [](int index)
{
    //Bounds Check
    if(index < 0 || index >= numBooks)
        exit(1);
    return bookIDs[index];
}

/*
 * 
 */
int main(int argc, char** argv) {
    
    Student s;
    
    s.addBook(3);
    s.addBook(2000);
    s.addBook(12);
    
    cout <<s[i] << endl; // Returns 2000

    return 0;
}

